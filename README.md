## Image Resizing Demo

Codebase is divided into two parts:
- Frontend (react)
  - To start frontend run following commands:
    - yarn install
    - yarn start
  - Application will normally be started at
    - http://localhost:3000


- Backend (express)
  - To start backend server run following commands:
    - cd server
    - yarn install
    - yarn start

  - Backend has two APIs
    - http://localhost:8080/images/fetch?url=IMAGE_URL
      - This will be used to fetch the Images from URL and will be returned without any resizing, and is currently used it in frontend.
      Resizing will be done on the frontend with the help of `canvas`.
      - Purpose of this API is prevent CORS issue on the browser.
      
    - http://localhost:8080/images/resize?url=IMAGE_URL
      - This API will resize the provided image as URL to maximum of 200 px of height and width, and maintaining their aspect ratio with the help of `sharp` npm package. Currently this API is not used in the frontend.
  
- Sample Video
  - https://www.useloom.com/share/977a06bcd8bc425fa57f98174c634169
