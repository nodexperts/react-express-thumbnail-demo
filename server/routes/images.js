const express = require('express');
const request = require('request');

const sharp = require('sharp');

const router = express.Router();

const ua = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36';

const customHeaderRequest = request.defaults({
  headers: { 'User-Agent': ua },
});

/* GET users listing. */
router.get('/fetch', (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');

  if (!req.query.url) {
    res.status(406).json({
      error: 'true',
      message: 'Please provide valid URL',
    });
  } else {
    try {
      customHeaderRequest.get(req.query.url).pipe(res);
    } catch (e) {
      res.status(406).json({
        error: 'true',
        message: 'Please provide valid URL',
      });
    }
  }
});

/* GET users listing. */
router.get('/resize', async (req, res) => {
  res.header('Access-Control-Allow-Origin', '*');

  if (!req.query.url) {
    res.status(406).json({
      error: 'true',
      message: 'Please provide valid URL',
    });
  } else {
    try {
      customHeaderRequest.get({ url: req.query.url, encoding: null }, (error, response, body) => {
        const width = 200;
        const height = 200;
        if (error) {
          res.status(406).json({
            error: 'true',
            message: 'Please provide valid URL',
          });

          return;
        }

        sharp(body)
          .resize(height, width)
          .max()
          .toBuffer()
          .then((data) => {
            // To display the image
            res.writeHead(200, {
              'Content-Type': 'image/png',
              'Content-Length': data.length,
            });

            return (res.end(data));
          })
          .catch((e) => {
            res.status(406).json({
              error: 'true',
              message: `Please provide valid URL: ${e.message}`,
            });
          });
      });
    } catch (e) {
      res.status(406).json({
        error: 'true',
        message: 'Please provide valid URL',
      });
    }
  }
});


module.exports = router;
