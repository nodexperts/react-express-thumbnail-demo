const express = require('express');

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const images = require('./routes/images');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/images', images);

module.exports = app;
