import styled from 'styled-components';

export default styled.label`
  display: block;
  padding: 50px;
  border-radius: 10px;
  border: 2px dotted black;
  text-align: center;
  margin: 10px;
  cursor: pointer;
`;
