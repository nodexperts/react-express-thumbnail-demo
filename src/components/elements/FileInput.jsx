import styled from 'styled-components';

export default styled.input.attrs({
  // we can define static props
  type: 'file',
})`
  display: none;
`;
