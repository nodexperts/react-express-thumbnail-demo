// Loading the image in using Image Object, and then using CANVAS to resize the image.

export const renderFromUrl = ({ src, width, height }, cb) => {
  const image = new Image();

  image.crossOrigin = 'anonymous';

  image.onerror = (error) => {
    cb(error);
  };

  image.onload = () => {
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');

    const oCanvas = document.createElement('canvas');
    const oContext = oCanvas.getContext('2d');

    if (image.width > image.height) {
      canvas.width = width; // destination canvas size
      canvas.height = width * (image.height / image.width);
    } else {
      canvas.height = height; // destination canvas size
      canvas.width = height * (image.width / image.height);
    }

    let cur = {
      width: canvas.width,
      height: canvas.height,
    };

    oCanvas.width = cur.width;
    oCanvas.height = cur.height;

    oContext.drawImage(image, 0, 0, cur.width, cur.height);

    // Loop is used to prevent the distortion
    while (cur.width * 0.5 > width && cur.height * 0.5 > height) {
      cur = {
        width: Math.floor(cur.width * 0.5),
        height: Math.floor(cur.height * 0.5),
      };
      oContext.drawImage(oCanvas, 0, 0, cur.width * 2, cur.height * 2, 0, 0, cur.width, cur.height);
    }

    ctx.drawImage(oCanvas, 0, 0, cur.width, cur.height, 0, 0, canvas.width, canvas.height);

    cb(null, canvas.toDataURL());
  };

  image.src = src;
};

// Here SRC is the file INPUT files
export const renderFromFileInput = ({ src, width, height }, cb) => {
  const reader = new FileReader();

  reader.onload = (event) => {
    renderFromUrl({
      src: event.target.result,
      width,
      height,
    }, cb);
  };

  reader.readAsDataURL(src);
};
