import React, { Component } from 'react';
import config from '../../configs/config';
import endpoints from '../../configs/endpoints';

import { FileLabel, FileInput, Separator, Input } from '../../components/elements';
import CenterAlign from '../../components/CenterAlign';
import Error from '../../components/Error';

import utils from '../../utils';

import { THUMB_DIMENSIONS } from '../../configs/constants';

class Thumbnail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      image: null,
    };
  }

  uploadChangeHandler = (e) => {
    this.resetHandler();

    const src = e.target.files[0];
    if (!src) {
      return;
    }

    // validation to Check whether selected file is a valid image or not
    if (!src.type.match(/image.*/)) {
      this.setState({
        error: 'Please select a valid image',
        image: null,
      });

      return;
    }

    // No need for CORS in this case, therefore, we can directly use our custom library.
    // Resizing the Image using canvas
    utils.imageProcessors.renderFromFileInput({
      src,
      width: THUMB_DIMENSIONS.width,
      height: THUMB_DIMENSIONS.height,
    }, this.callbackHandler);
  }

  urlChangeHandler = async (e) => {
    this.resetHandler();

    const url = e.target.value;

    // Due to CORS, we have to use the proxy, therefore requesting the Images from the node Server
    utils.imageProcessors.renderFromUrl({
      src: `${config.API_SERVER}${endpoints.FETCH_IMAGE}?url=${url}`,
      width: THUMB_DIMENSIONS.width,
      height: THUMB_DIMENSIONS.height,
    }, this.callbackHandler);
  };

  resetHandler = () => {
    this.setState({
      image: null,
      error: null,
    });
  }

  callbackHandler = (err, image) => {
    if (err) {
      this.setState({
        image: null,
        error: 'Oops, something is wrong with provided input!',
      });

      return;
    }

    this.setState({
      image,
      error: null,
    });
  }

  render() {
    const { error, image } = this.state;

    return (
      <CenterAlign>
        <FileLabel>
          <FileInput onChange={this.uploadChangeHandler} />
          Click here to upload
        </FileLabel>
        <Separator>
          OR
        </Separator>

        <Input placeholder="Enter URL of the image to preview" onBlur={this.urlChangeHandler} />

        { error && <Error>{error}</Error> }

        <div>
          { image && <img src={image} alt="Invalid URL" /> }
        </div>
      </CenterAlign>
    );
  }
}

export default Thumbnail;
