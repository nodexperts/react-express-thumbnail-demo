export const THUMB_DIMENSIONS = Object.freeze({
  width: 200,
  height: 200,
});
